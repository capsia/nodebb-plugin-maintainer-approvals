<form id="projects-form">

	<div class="panel panel-default">
		<div class="panel-heading">Assign a project</div>
		<div class="panel-body">

			<div class="form-group">
				<label for="projects-assign-list">Project to assign:</label>
				<input type="text" class="form-control" name="projects-assign-id" id="projects-assign-id"/>
			</div>

			<div class="form-group">
				<label for="projects-assign-user">User to assign:</label>
				<input type="text" class="form-control" name="projects-assign-user" id="projects-assign-user"/>
			</div>

			<div class="form-group">
				<label for="projects-assign-role">Role to assign:</label>
				<input type="text" class="form-control" name="projects-assign-role" id="projects-assign-role"/>
			</div>

			<div class="form-group">
				<label for="projects-assign-expire">Expiry time (in months, 0 if doesn't expire):</label>
				<input type="number" class="form-control" name="projects-assign-expire" id="projects-assign-expire"/>
			</div>

			<button type="button" id="projects-assign" class="btn btn-success btn-lg btn-block"><i class="fa fa-project"></i> Assign!</button>

		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">Project list</div>
		<div class="panel-body" id="projects-total-list">
			<!-- BEGIN projects -->
			<div class="projects-item panel panel-default" title="{projects.projectId}">
				<span class="projects-item-name panel-heading">{projects.projectId}</span>
				<button
					type="button"
					class="project-delete btn btn-danger btn-xs"
					data-projectsid="{projects.projectId}"
				>
					<i class="fa fa-trash"></i> Delete project
				</button>
				<div class="panel-body">
					{{{ each projects.contributors }}}
						<div>
							<span class="projects-item-name">{../user}</span>
							<span> is </span>
							<span class="projects-item-name">{../role}</span>
							<span> until </span>
							<span class="date-render projects-item-name" data-time="{../expires}">{../expires}</span>
							<button 
								type="button"
								class="assignment-expire-now btn btn-warning btn-xs"
								data-projectsid="{projects.projectId}"
								data-userslug="{../user}"
							>
								<i class="fa fa-clock-o"></i> Expire now
							</button>
							<button 
								type="button"
								class="assignment-delete btn btn-warning btn-xs"
								data-projectsid="{projects.projectId}"
								data-userslug="{../user}"
							>
								<i class="fa fa-trash"></i> Delete
							</button>
						</div>
					{{{ end }}}
				</div>
			</div>
			<!-- END projects -->
	</div>
</form>

<script>

	// Load data from API
	require(['settings'], function(Settings) {
		Settings.load('projects', $('#projects-form'));

		// Assign a user to a project given its userslug, projectslug and role, expiry is optional
		$('#projects-assign').on('click', function(ev) {

			// Get the data from the form
			var data = {
				project: $("#projects-assign-id").val(),
				user: $("#projects-assign-user").val(),
				role: $("#projects-assign-role").val(),
				expire: $("#projects-assign-expire").val()
			};

			// Verify data before sending to backend
			if (!data.project || !data.user || !data.role) {
				app.alertError("Check data!");
				ev.preventDefault();
				return false;
			};

			// Send data to backend for processing and saving
			socket.emit('admin.plugins.MaintainerPlugin.assignProject', data, function(err) {
				// Print the response
				if (err && err.hasOwnProperty("message")) {
					app.alertError("Error: " + err.message);
				} else {
					app.alertSuccess("User " + data.user + " assigned!");
					app.flags._unsaved = false;
					window.location.reload(false);
				}
			});
			ev.preventDefault();
			return false;
		});

		// Delete selected project
		$('.project-delete').click(function(event) {
			socket.emit('admin.plugins.MaintainerPlugin.deleteProject', $(this).data("projectsid"), function(result) {
				app.alertSuccess("Deleted project.");
				window.location.reload(false);
			});
		});

		// Delete a user from project
		$('.assignment-delete').click(function(event) {
			socket.emit('admin.plugins.MaintainerPlugin.deleteAssignment', {
				projectId: $(this).data("projectsid"),
				user: $(this).data("userslug")
			}, function(result) {
				app.alertSuccess("Deleted assignment.");
				window.location.reload(false);
			});
		});

		// Expire an assigned user now
		$('.assignment-expire-now').click(function(event) {
			socket.emit('admin.plugins.MaintainerPlugin.expireAssignment', {
				projectId: $(this).data("projectsid"),
				user: $(this).data("userslug")
			}, function(result) {
				app.alertSuccess("Expired assignment.");
				window.location.reload(false);
			});
		});

		// Render dates in readable format and colorize expired and expiring assignments
		$('.date-render').each(function() {
			if ($(this).data("time")) {
				let targetDate = new Date($(this).data("time"));
				$(this).text( targetDate.toDateString() );
				$(this).css("color", 
					targetDate.getTime() < new Date().getTime() ? "red" :
					Math.ceil(Math.abs(targetDate - new Date()) / ( 1000 * 60 * 60 * 24)) < 40 ? "orange" :
					""
				);
			} else {
				$(this).text("forever"); // If no data is set, the assignment doesn't expire
			}
		});

	});
</script>
