"use strict";

var	user = require.main.require('./src/user'),
	SocketAdmin = require.main.require('./src/socket.io/admin').plugins,
	db = require.main.require('./src/database'),
	async = require('async'),
	app;

var MaintainerPlugin = {};

MaintainerPlugin.onLoad = function(application, callback) {
	// Set managed routes
	application.router.get('/admin/plugins/maintainership', application.middleware.admin.buildHeader, renderAdmin);
	application.router.get('/api/admin/plugins/maintainership', renderAdmin);
	application.router.get('/api/plugins/maintainership/project/:projectslug', renderJSON);

	// Set functions that can be called from the frontend
	SocketAdmin.MaintainerPlugin = {
		deleteProject: MaintainerPlugin.deleteProject,
		deleteAssignment: MaintainerPlugin.deleteAssignment,
		expireAssignment: MaintainerPlugin.expireAssignment,
		assignProject: MaintainerPlugin.assignProject,
	};

	app = application.app; // Save for later
	callback();
};

// Add navigation to nodeBB
MaintainerPlugin.addAdminNavigation = function(nav, callback) {
	nav.plugins.push({
		"route": "/plugins/maintainership",
		"icon": "fa fa-certificate",
		"name": "Maintainer Plugin"
	});

	callback(null, nav);
};

// Socket functions

// Completely delete a project
MaintainerPlugin.deleteProject = function(socket, data, callback) {
	async.parallel([
		function(next) {
			db.setRemove("maintainership-plugin:projects", data, next);
		},
		function(next) {
			db.delete("maintainership-plugin:project:" + data, next);
		}
	], callback);
}

// Delete user from project
MaintainerPlugin.deleteAssignment = function(socket, data, callback) {
	db.getObject("maintainership-plugin:project:" + data.projectId, function(err, project) {
		if (err) return callback(err);

		project.contributors = project.contributors.filter(c => c.user != data.user);
		db.setObject("maintainership-plugin:project:" + data.projectId, project, callback);
	});
}

// Mark assignment period as expired for given user in given project
MaintainerPlugin.expireAssignment = function(socket, data, callback) {
	db.getObject("maintainership-plugin:project:" + data.projectId, function(err, project) {
		if (err) return callback(err);

		let assigneeNumber = project.contributors.findIndex(c => c.user == data.user);
		project.contributors[assigneeNumber].expires = new Date();
		db.setObject("maintainership-plugin:project:" + data.projectId, project, callback);
	});
}

MaintainerPlugin.assignProject = function(socket, data, callback) {
	// SAMPLE DATA: { project: 'developer_devices_hammerhead', user: 'toxuin', role: 'maintainer' }

// Check for the userslug
	user.getUidByUserslug(data.user, function(err, uid) {
		if (err) return callback(err);
		if (!uid) return callback(new Error("No such user!"));
		if (!data.project) return callback(new Error("Undefined project!"));
		if (!data.role) return callback(new Error("Undefined role!"));

// Check if the project already exists
		db.isSetMember("maintainership-plugin:projects", data.project, function(err, result) {
			async.parallel([
				function(next) {
					if (!result) // If the project doesn't yet exist create it, otherwise do nothing
						db.setAdd("maintainership-plugin:projects", data.project, next);
					else
						next();
				},
				function(next) {
					if (!result) // If the project was now created add only the currently set contributor
						db.setObject("maintainership-plugin:project:" + data.project, {
							projectId: data.project,
							contributors: [
								{
									user: data.user,
									role: data.role,
									expires: parseInt(data.expire) ? new Date(new Date().setMonth(new Date().getMonth()+parseInt(data.expire))) : null
								}
							]
						}, next);
					else
						db.getObject("maintainership-plugin:project:" + data.project, function(err, project) {
							if (err) return callback(err);

							let assigneeNumber = project.contributors.findIndex(c => c.user == data.user);
							if (assigneeNumber == -1) // Add new role
								project.contributors.push({
									user: data.user,
									role: data.role,
									expires: parseInt(data.expire) ? new Date(new Date().setMonth(new Date().getMonth()+parseInt(data.expire))) : null
								});
							else // Role is already present, renew the role instead
								project.contributors[assigneeNumber] = {
									user: data.user,
									role: data.role,
									expires: parseInt(data.expire) ? new Date(new Date().setMonth(new Date().getMonth()+parseInt(data.expire))) : null
								}
							db.setObject("maintainership-plugin:project:" + data.project, project, next);
						});
				}
			], callback);
		});
	});
}

// PRIVATE METHODS

// Render the administrator page
function renderAdmin(req, res, next) {
	async.parallel({
		projects: function(callback) { // Get all projects from the database
			getAllProjects(callback);
		}
	}, function(err, result) {
		res.render('admin/plugins/maintainership', result);
	});
}

// Get the specified project json and send it in the api
function renderJSON(request, response, callback) {
	if (!request.params || !request.params.projectslug) response.json({error: "No projectslug specified"});
	var projectslug = request.params.projectslug;

	db.getObject("maintainership-plugin:project:" + projectslug, function(err, project) {
		if (err) response.json({error: err});
		else response.json(project);
	});
}

// List all projects from db
function getAllProjects(callback) {
	db.getSetMembers('maintainership-plugin:projects', function(err, projectIds) {
		var projects = [];
		async.each(projectIds, function(projectId, next) {
			db.getObject("maintainership-plugin:project:" + projectId, function(err, project) {
				console.log(project);
				projects.push(project);
				next();
			});
		}, function(err) {
			callback(err, projects);
		});
	});
}

// Export the plugin to NodeBB
module.exports = MaintainerPlugin;
