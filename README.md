Maintainership Plugin for NodeBB
================

A plugin that allows to assign people as maintainers of a project in the nodeBB forum.

It was developed for use on [UBports Forums](https://forums.ubports.com/).

## Setup

First, install it to your NodeBB instance. You can do it from the admin interface or you can install it with

```bash
npm install nodebb-plugin-maintainer-approvals
```

from your nodebb install directory.

## License

[MIT](LICENSE)

## Credits

Based on nodebb-plugin-trophies-updated by mudkipme and toxuin
